import 'package:flutter/material.dart';
import 'package:flutter_learning/src/app.dart';
import 'package:flutter_learning/src/di/injection_container.dart' as di;
import 'package:flutter_learning/src/network/api/api_constant.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.initInjection(ApiConstant.baseUrl);
  runApp(const App());
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_learning/src/features/home/bloc/home_bloc.dart';
import 'package:flutter_learning/src/features/home/bloc/home_event.dart';
import 'package:flutter_learning/src/features/home/bloc/home_state.dart';
import 'package:flutter_learning/src/features/movie/ui/movie_list_screen.dart';
import 'package:rxdart/rxdart.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = '/home';

  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late final HomeBloc bloc;

  // int number = 0;
  bool checkbox = false;
  bool bookmark = false;

  // final _number = BehaviorSubject<int>.seeded(50);
  //
  // Stream<int> get number => _number.stream;
  //
  // Function(int) get addNumber => _number.add;
  //
  // int get numberValue => _number.valueOrNull ?? 0;

  @override
  void initState() {
    super.initState();
    // bloc = context.read<HomeBloc>();
    bloc = HomeBloc();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listenWhen: (prev, curr) => prev.isLoading != curr.isLoading,
      listener: (context, state) {
        if (state.isLoading) {
          print('ISLOADING TRUE');
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Home Screen'),
          actions: [
            IconButton(
              onPressed: () {
                setState(() {
                  bookmark = !bookmark;
                });
              },
              icon: Icon(bookmark ? Icons.bookmark : Icons.bookmark_outline),
            ),
            IconButton(
              onPressed: () {
                bloc.add(HomeEventNotificationClicked(isLoading: false));
              },
              icon: Icon(Icons.notifications),
            ),
            IconButton(
              onPressed: () {
                //function
              },
              icon: Icon(Icons.email_outlined),
            ),
            SizedBox(width: 10),
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            BlocBuilder<HomeBloc, HomeState>(
              bloc: bloc,
              builder: (context, state) {
                return Text(
                  '${state.number}',
                  style: TextStyle(fontSize: 100),
                  textAlign: TextAlign.center,
                );
              },
              buildWhen: (prev, curr) => prev.number != curr.number,
            ),
            // StreamBuilder<int>(
            //   stream: number,
            //   builder: (context, snapshot) {
            //     return Text(
            //       '${snapshot.data ?? 0}',
            //       style: TextStyle(fontSize: 100),
            //       textAlign: TextAlign.center,
            //     );
            //   },
            // ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () {
                    print('- button clicked');
                    bloc.add(HomeEventDecrement());
                    // addNumber(numberValue - 1);
                    // setState(() {
                    //   number--;
                    // });
                  },
                  child: Text('-'),
                ),
                SizedBox(width: 16),
                ElevatedButton(
                  onPressed: () {
                    print('+ button clicked');
                    bloc.add(HomeEventIncrement());
                    // addNumber(numberValue + 1);
                  },
                  child: Text('+'),
                ),
              ],
            ),
            Checkbox(
              value: checkbox,
              onChanged: (value) {
                setState(() {
                  checkbox = value ?? false;
                });
              },
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pushNamed(context, MovieListScreen.routeName);
                },
                child: Text('Go To Second Screen'),
              ),
            ),
          ],
        ),

        // body: Container(
        //   padding: EdgeInsets.all(20),
        //   decoration: BoxDecoration(
        //     color: Colors.blue,
        //     borderRadius: BorderRadius.circular(20),
        //   ),
        //   child: Text(
        //     'Home Screen',
        //     style: TextStyle(
        //       fontWeight: FontWeight.bold,
        //       fontSize: 30.5,
        //       color: Colors.white,
        //     ),
        //   ),
        // ),
      ),
    );
  }
}

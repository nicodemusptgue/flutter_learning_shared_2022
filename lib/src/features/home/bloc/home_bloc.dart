import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_learning/src/features/home/bloc/home_event.dart';
import 'package:flutter_learning/src/features/home/bloc/home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeState()) {
    on<HomeEventIncrement>(_increment);
    on<HomeEventDecrement>(_decrement);
    on<HomeEventNotificationClicked>(_notificationClicked);
  }

  _increment(HomeEventIncrement event, Emitter<HomeState> emit) {
    final number = state.number;
    emit(state.copyWith(number: number + 1));
  }

  _decrement(HomeEventDecrement event, Emitter<HomeState> emit) {
    final number = state.number;
    emit(state.copyWith(number: number - 1));
  }

  _notificationClicked(HomeEventNotificationClicked event, Emitter<HomeState> emit) async {
    await Future.delayed(Duration(seconds: 2));
    emit(state.copyWith(isLoading: event.isLoading));
  }
}

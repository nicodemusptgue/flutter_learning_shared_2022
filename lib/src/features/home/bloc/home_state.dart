import 'package:equatable/equatable.dart';

class HomeState extends Equatable {
  final bool isLoading;
  final int number;

  HomeState({
    this.number = 0,
    this.isLoading = false,
  });

  HomeState copyWith({
    int? number,
    bool? isLoading,
  }) {
    return HomeState(
      number: number ?? this.number,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  @override
  List<Object?> get props => [number, isLoading];
}

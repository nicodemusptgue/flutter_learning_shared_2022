import 'package:equatable/equatable.dart';
import 'package:flutter_learning/src/network/model/movie/movie_list_response_model.dart';

enum MovieListStates {
  onLoading,
  onLoaded,
  onError,
}

class MovieListState extends Equatable {
  final MovieListStates state;
  final String errorMessage;
  final List<MovieListModel> movieList;

  MovieListState({
    this.state = MovieListStates.onLoading,
    this.errorMessage = '',
    this.movieList = const [],
  });

  MovieListState copyWith({
    MovieListStates? state,
    String? errorMessage,
    List<MovieListModel>? movieList,
  }) {
    return MovieListState(
      state: state ?? this.state,
      errorMessage: errorMessage ?? '',
      movieList: movieList ?? this.movieList,
    );
  }

  @override
  List<Object?> get props => [state, errorMessage, movieList];
}

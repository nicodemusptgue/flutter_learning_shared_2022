import 'package:dio/dio.dart';

class DioClient {
  final String baseUrl;

  DioClient({required this.baseUrl});

  Dio get dio => _getDio();

  Dio _getDio() {
    BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 50000,
      receiveTimeout: 50000,
    );

    Dio dio = Dio(options);
    dio.interceptors.addAll([_interceptor()]);
    return dio;
  }

  Interceptor _interceptor() {
    return InterceptorsWrapper(onRequest: (options, handler) {
      options.queryParameters['api_key'] = '201c75114cecb3836c33f418b2ee9620';
      handler.next(options);
    });
  }
}

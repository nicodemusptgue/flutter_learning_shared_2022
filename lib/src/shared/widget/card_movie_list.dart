import 'package:flutter/material.dart';

class CardMovieList extends StatelessWidget {
  final String? title;
  final String? cover;
  final String? releaseDate;
  final double? voteAverage;
  final Function()? onTap;

  const CardMovieList({
    Key? key,
    this.title,
    this.cover,
    this.releaseDate,
    this.voteAverage,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      key: const Key('CARD_MOVIE_LIST'),
      onTap: onTap,
      child: Material(
        elevation: 1,
        borderRadius: BorderRadius.circular(16),
        child: Container(
          padding: EdgeInsets.all(16),
          child: Row(
            children: [
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.grey,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      'https://www.themoviedb.org/t/p/w440_and_h660_face' + (cover ?? ''),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.star,
                          color: Colors.orange,
                        ),
                        Text(' ${voteAverage ?? '0'} / 10')
                      ],
                    ),
                    Text(
                      title ?? '-',
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Text(
                      releaseDate ?? '-',
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

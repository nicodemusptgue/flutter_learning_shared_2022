import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_learning/src/features/home/bloc/home_bloc.dart';
import 'package:flutter_learning/src/features/home/ui/home_screen.dart';
import 'package:flutter_learning/src/features/movie/argument/movie_detail_argument.dart';
import 'package:flutter_learning/src/features/movie/ui/movie_detail_screen.dart';
import 'package:flutter_learning/src/features/movie/ui/movie_list_screen.dart';
import 'package:flutter_learning/src/shared/util/extension.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(
          create: (context) => HomeBloc(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Learning',
        initialRoute: HomeScreen.routeName,
        routes: {
          HomeScreen.routeName: (context) => const HomeScreen(),
          MovieListScreen.routeName: (context) => const MovieListScreen(),
          MovieDetailScreen.routeName: (context) =>
              MovieDetailScreen(argument: context.argument<MovieDetailArgument>()),
        },
      ),
    );
  }
}

import 'package:dio/dio.dart';
import 'package:flutter_learning/src/network/model/movie/movie_list_response_model.dart';
import 'package:flutter_learning/src/repository/interface/movie_repository.dart';

class MovieApiRepository implements MovieRepository {
  final Dio dio;

  MovieApiRepository({required this.dio});

  @override
  Future<MovieListResponseModel> movieGetList({int? page}) async {
    try {
      final response = await dio.get('movie/popular/', queryParameters: {
        'page': page,
      });
      return MovieListResponseModel.fromJson(response.data);
    } on DioError catch (e) {
      throw Exception(e.error);
    }
  }
}

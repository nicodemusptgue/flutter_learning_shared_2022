import 'package:flutter_learning/src/network/model/movie/movie_list_response_model.dart';

abstract class MovieRepository {
  Future<MovieListResponseModel> movieGetList({int? page});
}

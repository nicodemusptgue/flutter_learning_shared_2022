import 'package:flutter_learning/src/features/movie/bloc/movieList/movie_list_bloc.dart';
import 'package:flutter_learning/src/network/api/dio_client.dart';
import 'package:flutter_learning/src/repository/interface/movie_repository.dart';
import 'package:flutter_learning/src/repository/remote/movie_api_repository.dart';
import 'package:get_it/get_it.dart';

final inject = GetIt.instance;

Future<void> initInjection(String baseUrl) async {
  inject.registerFactory(() => MovieListBloc());

  //REPOSITORY
  inject.registerLazySingleton<MovieRepository>(() => MovieApiRepository(dio: inject()));

  //NETWORK
  inject.registerLazySingleton(() => inject<DioClient>().dio);
  inject.registerLazySingleton(() => DioClient(baseUrl: baseUrl));
}
